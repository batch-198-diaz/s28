// db.users.insertOne({
// 
//     "username": "daily_rock",
//     "password": "m3thr0ck"
// 
//     })

// insert one document
// db.users.insertOne({
// 
//     "username": "celtic_nerd",
//     "password": "Celts000"
// 
//     })

// insert multiple documents at once
// db.users.insertMany(
//     [
//         {
//             "username": "pablo123",
//             "password": "123paul"
//         },
//         {
//             "username": "pedro99",
//             "password": "iampeter99"           
//         }
//     ]
// )
// 
// db.products.insertMany(
//         [
//             {
//                 "name": "product_1",
//                 "description": "electronics",
//                 "price": 10000
//             },
//             {
//                 "name": "product_2",
//                 "description": "beverages",
//                 "price": 200               
//             },
//             {
//                 "name": "product_3",
//                 "description": "video game",
//                 "price": 1500                
//             }
//         ]
//         
//         )

// Read/Retrieve >>  

// db.users.find() - returns all docu in collection

//db.collection.fing({"criteria":"value"}) - return/find all docu that match criteria

 
// db.cars.insertMany(
//     [
//         {
//            "name":"Vios",
//            "brand":"Toyota",
//            "type":"sedan",
//            "price":15000000            
//         },
//         {
//            "name":"Tamaraw FX",
//            "brand":"Toyota",
//            "type":"auv",
//            "price":750000               
//         },
//         {
//            "name":"City",
//            "brand":"Honda",
//            "type":"sedan",
//            "price":1600000
//         }
//     ]
// 
// )
// 

// db.users.find({"username":"pedro99"})
// db.cars.find({"type":"sedan"})
// db.cars.find({"brand":"Toyota"})
// db.cars.find({"_id": ObjectId("62e0aa65ebeeb10fd3a2c687")})

//db.cars.findOne({}) - first item/docu
// db.cars.findOne({"type":"sedan"})

// Update

// with criteria
//db.collection.updateOne(
//      {"criteria":"value"},
//      {$set:{
//              "field_to_be_updated": "updated_value"                
//            }
//        }        
//    ) 

// db.cars.updateOne({"name":"City"},{$set:{"price":1600000}})
//db.cars.findOne({})

// without criteria - will update first item in collection
// db.users.updateOne({},{$set:{"username":"Updated_Username"}})
// db.users.find({})

// db.users.updateOne({"username":"daily_rock"},{$set:{"isAdmin":true}})
// db.users.updateMany({},{$set:{"isAdmin":true}})

// db.cars.updateMany({"type":"sedan"},{$set:{"price":1000000}})


//Delete



//db.collection.deleteOne({}) - deletes first item in collection



//db.products.deleteOne({})







//db.collection.deleteOne({"criteria":"criteria"})



//deletes first item that matches criteria



//db.cars.deleteOne({"brand":"Toyota"})







//db.collection.deleteMany({"criteria":"value"})



//deletes all items that matches the criteria



//db.users.deleteMany({"isAdmin":true})







//db.collection.deleteMany({})



//delete all documents in a collection



//db.products.deleteMany({})



//db.cars.deleteMany({})


